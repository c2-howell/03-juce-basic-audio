/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    setSize (500, 400);
    gainSlider.setBounds((getWidth()/2)-200, (getHeight()/2)-35, 80, 80);
    gainSlider.setSliderStyle(Slider::Rotary);
    gainSlider.setRange(0, 1);
    gainSlider.addListener(this);
    gainSlider.setValue(0.75);
    addAndMakeVisible(&gainSlider);
    
    oscSelector.addItem("Sine", 1);
    oscSelector.addItem("Square", 2);
    oscSelector.addItem("Sawtooth", 3);
    oscSelector.addItem("Triangle", 4);
    oscSelector.setSelectedId(1,dontSendNotification);
    oscSelector.setBounds(getWidth()/2, getHeight()/2-25, 200, 50);
    oscSelector.addListener(this);
    addAndMakeVisible(oscSelector);
    
    gainLabel.setBounds((getWidth()/2)-200, (getHeight()/2)-75, 100, 80);
    oscLabel.setBounds(getWidth()/2, (getHeight()/2)-75, 100, 80);
    gainLabel.setText("Gain Control:", dontSendNotification);
    oscLabel.setText("Oscillator Select:", dontSendNotification);
    addAndMakeVisible(gainLabel);
    addAndMakeVisible(oscLabel);

}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    audio.getGain(gainSlider.getValue());
}

 void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    audio.getID(oscSelector.getSelectedId());
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

