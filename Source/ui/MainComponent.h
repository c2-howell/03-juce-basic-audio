/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"


//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */
class MainComponent   : public Component,
public MenuBarModel,
public Slider::Listener,
public ComboBox::Listener
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);
    
    /** Destructor */
    ~MainComponent();
    
    void resized() override;
    
    virtual void sliderValueChanged (Slider* slider);
    
    virtual void comboBoxChanged (ComboBox* comboBoxThatHasChanged);
    
    int getSR();
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
private:
    Audio& audio;
    Slider gainSlider;
    ComboBox oscSelector;
    Label gainLabel, oscLabel;
    
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
