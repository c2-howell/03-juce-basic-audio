/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"


Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    oscId=1;
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    if (message.isNoteOn(true)==true){
    int noteNum=message.getNoteNumber();
    sinOsc.setFreq(MidiMessage::getMidiNoteInHertz(noteNum));
        squareOsc.setFreq(MidiMessage::getMidiNoteInHertz(noteNum));
        sinOsc.setOnOffAmp(1);
        squareOsc.setOnOffAmp(1);
        triOsc.setOnOffAmp(1);
        sawOsc.setOnOffAmp(1);
    }
    if (message.getVelocity()==0){
        sinOsc.setOnOffAmp(0);
        squareOsc.setOnOffAmp(0);
    triOsc.setOnOffAmp(0);
    sawOsc.setOnOffAmp(0);}
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        if(oscId==1){
        *outL = sinOsc.getSample();
        *outR = sinOsc.getSample();
        }
        
        if(oscId==2){
            *outL = squareOsc.getSample();
            *outR = squareOsc.getSample();
        }
        
        if(oscId==3){
            *outL = sawOsc.getSample();
            *outR = sawOsc.getSample();
        }
        
        if(oscId==4){
            *outL = triOsc.getSample();
            *outR = triOsc.getSample();
        }
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sampleRate=device->getCurrentSampleRate();
    sinOsc.setSR(sampleRate);
    squareOsc.setSR(sampleRate);
    triOsc.setSR(sampleRate);
    sawOsc.setSR(sampleRate);
    
 
}

void Audio::audioDeviceStopped()
{

}

void Audio::getGain(float gain)
{
    sinOsc.setAmp(gain);
    squareOsc.setAmp(gain);
    triOsc.setAmp(gain);
    sawOsc.setAmp(gain);
    
}

void Audio::getID(int iD)
{
   oscId=iD;
}


