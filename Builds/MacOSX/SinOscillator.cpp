//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#include "SinOscillator.h"



float SinOscillator::renderWaveShape(const float currentPhase)
{
    return sin (currentPhase);
    
}


