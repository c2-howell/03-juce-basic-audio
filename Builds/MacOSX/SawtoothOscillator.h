//
//  SawtoothOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 05/01/2015.
//
//

#ifndef __JuceBasicAudio__SawtoothOscillator__
#define __JuceBasicAudio__SawtoothOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"


class SawtoothOscillator : public Oscillator
{
public:
    
    float renderWaveShape(const float currentPhase) override;
    
};

#endif /* defined(__JuceBasicAudio__SawtoothOscillator__) */
