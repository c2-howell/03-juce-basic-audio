//
//  SqaureOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__SqaureOscillator__
#define __JuceBasicAudio__SqaureOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

class SquareOscillator : public Oscillator
{
public:
    
    float renderWaveShape(const float currentPhase) override;
    
};
#endif /* defined(__JuceBasicAudio__SqaureOscillator__) */
