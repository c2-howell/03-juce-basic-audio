//
//  TriangleOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__TriangleOscillator__
#define __JuceBasicAudio__TriangleOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"


class TriangleOscillator : public Oscillator
{
public:
    
    float renderWaveShape(const float currentPhase) override;
    
};
#endif /* defined(__JuceBasicAudio__TriangleOscillator__) */
