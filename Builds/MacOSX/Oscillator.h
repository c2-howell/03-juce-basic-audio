//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__Oscillator__
#define __JuceBasicAudio__Oscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

class Oscillator
{
public:
    
    Oscillator();
    
    
   virtual ~Oscillator();
    
    //float getSample();
    
    void setFreq(float freq);
    
    void setAmp(float amp);
    
    void setOnOffAmp(float amp);
    
    void setSR(float sr);
    
        
    virtual float renderWaveShape(const float currentPhase) = 0;
    
    float getSample();
    
    
    
    
private:
    float gain, onOffGain;
    float frequency;
    float phasePos, phaseInc;
    float sampleRate;
    float currentSample;
    
};


#endif /* defined(__JuceBasicAudio__Oscillator__) */
