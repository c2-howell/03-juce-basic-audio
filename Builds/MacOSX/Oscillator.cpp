//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#include "Oscillator.h"

Oscillator::Oscillator()
{
    gain=0.f;
    frequency=0.f;
    phasePos=0.f;
    
}

Oscillator::~Oscillator()
{
    
}

void Oscillator::setSR(float sr)
{
    sampleRate=sr;
}

void Oscillator::setFreq(float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI *  frequency ) / sampleRate ;
    
}

void Oscillator::setAmp(float amp)
{
    gain=amp;
}

void Oscillator::setOnOffAmp(float amp)
{
    onOffGain=amp;
}



float Oscillator::getSample()
{
    float out = renderWaveShape (phasePos ) * gain * onOffGain;
	phasePos = phasePos + phaseInc ;
	if(phasePos  > (2.f * M_PI))
		phasePos -= (2.f * M_PI);
    
	return out;
    
    
}


