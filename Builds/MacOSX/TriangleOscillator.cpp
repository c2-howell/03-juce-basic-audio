//
//  TriangleOscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#include "TriangleOscillator.h"

float TriangleOscillator::renderWaveShape(const float currentPhase)
{
   return sin (currentPhase);  
}