//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__SinOscillator__
#define __JuceBasicAudio__SinOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"


class SinOscillator : public Oscillator
{
public:
    
    float renderWaveShape(const float currentPhase) override;
  
};
#endif /* defined(__JuceBasicAudio__SinOscillator__) */
